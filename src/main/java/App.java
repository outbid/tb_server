import battle.BattlesController;
import controller.RequestController;
import org.java_websocket.WebSocketImpl;
import utils.D;
import websocket.TurnBaseServer;

import java.net.UnknownHostException;

public class App {

    public static void main(String[] args) {
        WebSocketImpl.DEBUG = false;
        int port = 8887; // 843 flash policy port
        TurnBaseServer s;
        BattlesController bc;
        try {
            bc = new BattlesController();
            bc.start();

            s = new TurnBaseServer(port);
            s.setRequestController(new RequestController());
            s.start();
            D.debug("websocket.TurnBaseServer started on port: " + s.getPort());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

}
