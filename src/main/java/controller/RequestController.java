package controller;

import battle.BattlesController;
import battle.exceptions.UserIsNullException;
import controller.exceptions.RequestParserException;
import controller.interfaces.IRequestController;
import net.minidev.json.JSONObject;
import net.minidev.json.JSONStyle;
import net.minidev.json.JSONValue;
import net.minidev.json.parser.ParseException;
import org.java_websocket.WebSocket;
import users.User;
import users.Users;
import utils.D;
import websocket.Request;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class RequestController implements IRequestController {

    public void processRequest(WebSocket webSocket, String data) {
        new JSONStyle(JSONStyle.FLAG_PROTECT_4WEB);
        JSONObject request;
        try {
            request = (JSONObject) JSONValue.parseStrict(data);
        } catch (ParseException e) {
            e.printStackTrace();
            webSocket.send("RequestController.processRequest: invalid json data: " + e.getMessage());
            throw new RequestParserException("RequestController.processRequest: invalid json data: " + e.getMessage());
        } catch (ClassCastException e) {
            e.printStackTrace();
            webSocket.send("RequestController.processRequest: invalid json data: " + e.getMessage());
            throw new RequestParserException("invalid json data: " + e.getMessage());
        }

        D.debug(request);
        webSocket.send("data is correct: " + request.toString());
        String requestType = request.getAsString("type");
        if (requestType == null) {
            throw new RequestParserException("RequestController.processRequest: request type is null");
        }

        Users users = Users.getInstance();
        User user = null;
        try {
            user = users.getUserByWebSocket(webSocket);
        } catch (UserIsNullException e) {
            if (requestType.equals("auth")) {
                users.addUser(webSocket);
            } else {
                D.debug("RequestController.processRequest: user are not auth, request string: " + requestType);
                webSocket.send("need auth request first");
            }
        }
        if (user != null) {
            //String method = request.getAsString("action");
            //invoke(method);

            if(requestType.equals("battle")) {
                BattlesController.addData(new Request(user, request));
            }

        }

    }

    private void invoke(String method) {
        D.debug(method);
        try {
            Method m = this.getClass().getDeclaredMethod(method);
            m.setAccessible(true);
            m.invoke(this);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private void searchOpponent() {
        D.debug("RequestController.searchOpponent invoked!");
    }

    private void startBattle() {
        D.debug("RequestController.startBattle invoked");
    }

    private void endTurn() {
        D.debug("RequestController.endTurn invoked");
    }

}
