package controller.exceptions;

public class InvalidActionsException extends RuntimeException{
    public InvalidActionsException(String s) {
        super(s);
    }
}
