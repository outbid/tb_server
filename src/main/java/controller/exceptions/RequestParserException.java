package controller.exceptions;


public class RequestParserException extends RuntimeException {
    public RequestParserException(String s) {
        super(s);
    }
}
