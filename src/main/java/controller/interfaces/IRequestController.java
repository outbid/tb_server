package controller.interfaces;

import org.java_websocket.WebSocket;

public interface IRequestController {

    void processRequest(WebSocket conn, String data);

}
