package battle;


import battle.exceptions.BattleActionIsNullException;
import battle.exceptions.UserIsNullException;
import net.minidev.json.JSONObject;
import utils.D;
import websocket.Request;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class BattlesController extends Thread {

    private static BlockingQueue<Request> battleRequests = new ArrayBlockingQueue<Request>(500);

    private BattleQueue battleQueue;

    public BattlesController() {
        battleQueue = new BattleQueue(this);
    }

    public static void addData(Request request) {
        try {
            battleRequests.put(request);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        try {
            Request battleRequest;
            while (true) {
                try {
                    battleRequest = BattlesController.battleRequests.take();
                    if (battleRequest.getFrom() == null) {
                        throw new UserIsNullException("BattlesController.run user is null in BattleRequest object");
                    }
                    D.debug("have new request for BattleController " + battleRequest);

                    JSONObject rq = battleRequest.getRequest();
                    String action = rq.getAsString("action");
                    if (null == action) {
                        throw new BattleActionIsNullException("BattlesController.run JSONObject request.action must be a string");
                    }

                    if (action.equals("findOpponent")) {
                        battleQueue.addUser(battleRequest.getFrom());
                        D.debug("BattlesController.run have new findOpponent request");
                    } else if (action.equals("removeFromQueue")) {
                        battleQueue.removeUser(battleRequest.getFrom());
                    } else if (battleRequest.getFrom().inBattle()) {
                        battleRequest.getFrom().getBattleSession().processRequest(battleRequest);
                    }
                } catch (RuntimeException e) {
                    e.printStackTrace();
                    D.debug("BattlesController.run RuntimeException: " + e.getMessage());
                }
            }
        } catch (InterruptedException e) {
            D.error("Thread BattlesController closed. Reason: " + e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            D.error("Thread BattlesController closed. Reason: " + e.getMessage());
            e.printStackTrace();
        }

    }


}

