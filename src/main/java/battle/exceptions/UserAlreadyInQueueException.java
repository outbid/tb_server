package battle.exceptions;

public class UserAlreadyInQueueException extends RuntimeException {

    public UserAlreadyInQueueException(String s) {
        super(s);
    }

}
