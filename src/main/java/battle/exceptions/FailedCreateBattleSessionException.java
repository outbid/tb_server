package battle.exceptions;


public class FailedCreateBattleSessionException extends RuntimeException {

    public FailedCreateBattleSessionException(String s) {
        super(s);
    }

}
