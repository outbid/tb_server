package battle.exceptions;


public class BattleActionIsNullException extends RuntimeException {

    public BattleActionIsNullException(String s) {
        super(s);
    }

}
