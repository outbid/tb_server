package battle.exceptions;

public class UserIsNullException extends RuntimeException {
    public UserIsNullException(String s) {
        super(s);
    }

}
