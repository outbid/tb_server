package battle.Units;


import org.apache.commons.beanutils.BeanUtils;

import java.lang.reflect.InvocationTargetException;

public class BattleUnit {
    private int battleId;
    private int speed;
    private int damagePerShot;
    private int numberOfShots;
    private int firingRange;
    private int baseHp;
    private int currentHp;
    private int poxX;
    private int posY;

    public BattleUnit cloneUnit() {
        BattleUnit battleUnit = null;
        try {
            battleUnit = (BattleUnit) BeanUtils.cloneBean(this);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return battleUnit;
    }

    @Override
    public String toString() {
        return "BattleUnit{" +
                "speed=" + speed +
                ", damagePerShot=" + damagePerShot +
                ", numberOfShots=" + numberOfShots +
                ", firingRange=" + firingRange +
                ", baseHp=" + baseHp +
                ", currentHp=" + currentHp +
                ", poxX=" + poxX +
                ", posY=" + posY +
                '}';
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getDamagePerShot() {
        return damagePerShot;
    }

    public void setDamagePerShot(int damagePerShot) {
        this.damagePerShot = damagePerShot;
    }

    public int getNumberOfShots() {
        return numberOfShots;
    }

    public void setNumberOfShots(int numberOfShots) {
        this.numberOfShots = numberOfShots;
    }

    public int getFiringRange() {
        return firingRange;
    }

    public void setFiringRange(int firingRange) {
        this.firingRange = firingRange;
    }

    public int getBaseHp() {
        return baseHp;
    }

    public void setBaseHp(int baseHp) {
        this.baseHp = baseHp;
    }

    public int getCurrentHp() {
        return currentHp;
    }

    public void setCurrentHp(int currentHp) {
        this.currentHp = currentHp;
    }

    public int getPoxX() {
        return poxX;
    }

    public void setPoxX(int poxX) {
        this.poxX = poxX;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }

    public int getBattleId() {
        return battleId;
    }

    public void setBattleId(int battleId) {
        this.battleId = battleId;
    }
}
