package battle.interfaces;

import websocket.Request;

public interface BattleState {

    void processRequest(Request request);

}
