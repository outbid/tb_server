package battle;

import battle.Units.BattleUnit;
import net.minidev.json.JSONObject;
import users.User;

import java.util.List;

public class Combatant {


    private User user;


    private List<BattleUnit> units;
    private JSONObject lastRequest = null;

    public Combatant(User user, List<BattleUnit> units) {
        this.user = user;
        this.units = units;
    }

    public JSONObject getLastRequest() {
        return lastRequest;
    }

    public void setLastRequest(JSONObject lastRequest) {
        this.lastRequest = lastRequest;
    }

    public User getUser() {
        return user;
    }

    public List<BattleUnit> getUnits() {
        return units;
    }
}
