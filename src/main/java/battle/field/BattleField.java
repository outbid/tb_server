package battle.field;

import java.util.ArrayList;
import java.util.List;

public class BattleField {

    private List<List<FieldCell>> field = new ArrayList<List<FieldCell>>();

    public BattleField(int sizeX, int sizeY) {

        for (int i = 0; i < sizeY; i++) {

            field.add(new ArrayList<FieldCell>());
            for (int j = 0; j < sizeX; j++) {
                field.get(i).add(new FieldCell(j, i));
            }

        }

    }

    public FieldCell cell(int x, int y) {
        return field.get(y).get(x);
    }

}
