package battle;

import battle.Units.BattleUnit;
import battle.exceptions.UserIsNullException;
import battle.field.BattleField;
import battle.interfaces.BattleState;
import net.minidev.json.JSONObject;
import users.User;
import utils.D;
import websocket.Request;

import java.util.ArrayList;
import java.util.List;

public class BattleSession {
    private int turnNumber = 0;

    private BattleState battleState;

    private BattleField battleField = new BattleField(20, 20);

    private Combatant combatant1;
    private Combatant combatant2;


    public BattleSession(User user1, User user2) {

        if (null == user1) {
            throw new UserIsNullException("BattleSession constructor user1 is null");
        }
        if (null == user2) {
            throw new UserIsNullException("BattleSession constructor user2 is null");
        }

        List<BattleUnit> units1 = new ArrayList<BattleUnit>();
        List<BattleUnit> units2 = new ArrayList<BattleUnit>();

        BattleUnit battleUnit = new BattleUnit();
        battleUnit.setBaseHp(10);
        battleUnit.setCurrentHp(10);
        battleUnit.setDamagePerShot(3);
        battleUnit.setFiringRange(5);
        battleUnit.setNumberOfShots(5);
        battleUnit.setSpeed(7);

        for (int i = 0; i < 5; i++) {
            BattleUnit u = battleUnit.cloneUnit();
            u.setBattleId(i);
            units1.add(i, u);
        }
        for (int i = 0; i < 5; i++) {
            BattleUnit u = battleUnit.cloneUnit();
            u.setBattleId(i);
            units2.add(i, u);
        }

        this.combatant1 = new Combatant(user1, units1);
        this.combatant2 = new Combatant(user2, units2);


        D.debug("Battle session created with users: " + user1.getIp() + ", " + user2.getIp());
        user1.sendMsg("Battle started!");
        user2.sendMsg("Battle started!");
        startBattleResponse(combatant1, combatant2);
        startBattleResponse(combatant2, combatant1);

        battleState = new PrepareState();
    }

    public void processRequest(Request request) {
        battleState.processRequest(request);
    }

    private class PrepareState implements BattleState {

        public void processRequest(Request request) {
            Combatant c = getCombatant(request);
            c.setLastRequest(request.getRequest());
            List<BattleUnit> playerUnits = c.getUnits();

            if (combatant1.getLastRequest() != null && combatant2.getLastRequest() != null) {
                battleState = new TurnState();
                D.debug("BattleSession prepare state ends");
            }

        }
    }

    private class TurnState implements BattleState {

        public void processRequest(Request request) {
            turnNumber++;

            D.debug("BattleSession turn state number " + turnNumber + " ends");

        }
    }

    public void finishSession() {
        try {
            combatant1.getUser().setBattleSession(null);
            combatant1.getUser().sendMsg("battleSession finished");
        } catch (Exception e) {
            e.printStackTrace();
            D.debug("BattleSession.finishSession Exception " + e.getMessage());
        }

        try {
            combatant2.getUser().setBattleSession(null);
            combatant2.getUser().sendMsg("battleSession finished");
        } catch (Exception e) {
            e.printStackTrace();
            D.debug("BattleSession.finishSession Exception " + e.getMessage());
        }

    }

    private void startBattleResponse(Combatant player, Combatant enemy) {

        JSONObject json = new JSONObject();

        json.put("type", "battleInit");

        JSONObject data = new JSONObject();

        JSONObject battlefield = new JSONObject();
        battlefield.put("sizeX", 20);
        battlefield.put("sizeY", 20);
        data.put("battlefield", battlefield);

        JSONObject playerUnits = new JSONObject();
        for (BattleUnit u : player.getUnits()) {
            playerUnits.put(Integer.toString(u.getBattleId()), u);
        }
        data.put("playerUnits", playerUnits);

        JSONObject enemyUnits = new JSONObject();
        for (BattleUnit u : enemy.getUnits()) {
            enemyUnits.put(Integer.toString(u.getBattleId()), u);
        }
        data.put("enemyUnits", enemyUnits);

        json.put("data", data);

        player.getUser().sendMsg(json.toJSONString());

    }

    private Combatant getCombatant(Request request) {
        if (request.getFrom() == combatant1.getUser()) {
            return combatant1;
        } else if (request.getFrom() == combatant2.getUser()) {
            return combatant2;
        } else {
            throw new RuntimeException("BattleSession.getPlayer User request are not belongs to this battle session");
        }
    }

    private Combatant getEnemy(Request request) {
        if (request.getFrom() == combatant1.getUser()) {
            return combatant2;
        } else if (request.getFrom() == combatant2.getUser()) {
            return combatant1;
        } else {
            throw new RuntimeException("BattleSession.getEnemy User request are not belongs to this battle session");
        }
    }

}
