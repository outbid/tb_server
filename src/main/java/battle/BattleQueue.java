package battle;

import battle.exceptions.UserAlreadyInQueueException;
import battle.exceptions.UserIsNullException;
import users.User;
import users.exceptions.UserAlreadyInBattleException;
import utils.D;

import java.util.LinkedList;

public class BattleQueue {

    private BattlesController battlesController;
    private LinkedList<User> queue = new LinkedList<User>();

    public BattleQueue(BattlesController battlesController) {
        this.battlesController = battlesController;
    }

    public synchronized void addUser(User user) {
        if(null == user) {
            throw new UserIsNullException("BattleQueue.addUser param user is null");
        }

        if(user.inBattle()) {
            throw new UserAlreadyInBattleException("Can't add user[" + user.getIp() +"] to battle queue, he is already in battle");
        }

        if (queue.indexOf(user) != -1) {
            throw new UserAlreadyInQueueException("Can't add user into battle queue, user[" + user.getIp() +"] already in queue ");
        }

        queue.add(user);
        D.debug("User added to queue:" + user.getIp() + ", queue size: " + queue.size());
        tryStartBattleSession();
    }

    private void tryStartBattleSession() {
        if (queue.size() > 1) {
            if (null == queue.getFirst()) {
                throw new UserIsNullException("BattleQueue.tryStartBattleSession user1 is null");
            }
            if (null == queue.get(1)) {
                throw new UserIsNullException("BattleSession.tryStartBattleSession constructor user2 is null");
            }
            User user1 = queue.poll();
            User user2 = queue.poll();

            BattleSession battleSession = new BattleSession(user1, user2);

            user1.setBattleSession(battleSession);
            user2.setBattleSession(battleSession);

            D.debug("Queue updated, queue size: " + queue.size());
        } else {
            D.debug("BattleQueue.tryStartBattleSession battle not started, not enough users");
        }

    }

    public synchronized void removeUser(User user) {
        if(null == user) {
            throw new UserIsNullException("BattleQueue.removeUser param user is null");
        }
        int index = queue.indexOf(user);
        if (index != -1) {
            queue.remove();
            D.debug("User " + user + " removed from queue, queue size " + queue.size());
        } else {
            D.debug("BattleQueue.removeUser user no in queue");
        }
    }

}
