package users;

import battle.BattleSession;
import battle.BattlesController;
import net.minidev.json.JSONObject;
import net.minidev.json.JSONValue;
import net.minidev.json.parser.ParseException;
import org.java_websocket.WebSocket;
import users.exceptions.UsersWebSocketIsNullException;
import utils.D;
import websocket.Request;


public class User {

    private WebSocket webSocket;

    private BattleSession battleSession = null;


    public User(WebSocket webSocket) {
        this.webSocket = webSocket;
        D.debug("User created, user ip: " + webSocket.getRemoteSocketAddress());
    }

    public String getIp() {
        if (null == webSocket.getRemoteSocketAddress()) {
            throw new UsersWebSocketIsNullException("User.getIp users webSocket.getRemoteSocketAddress() returns null");
        }
        return webSocket.getRemoteSocketAddress().toString();
    }

    public WebSocket getWebSocket() {
        return webSocket;
    }

    public void sendMsg(String msg) {

        webSocket.send(msg);
    }

    public boolean inBattle() {
        return !(null == battleSession);
    }

    public BattleSession getBattleSession() {
        return battleSession;
    }

    public void setBattleSession(BattleSession battleSession) {
        this.battleSession = battleSession;
    }


    public void destroy() {

        //TODO: ��������� �������
        //���������� ������ ��� �������� ������������ �� �������
        String data = "{\"type\":\"battle\", \"action\":\"removeFromQueue\"}";
        JSONObject request = null;
        try {
            request = (JSONObject) JSONValue.parseStrict(data);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        D.debug("User.destroy " + data);
        BattlesController.addData(new Request(this, request));

        if (null != battleSession) {
            battleSession.finishSession();
        }

        battleSession = null;

        webSocket.close();
    }

    public String toString() {
        return webSocket.getRemoteSocketAddress().toString();
    }
}