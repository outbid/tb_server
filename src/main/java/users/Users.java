package users;


import battle.exceptions.UserIsNullException;
import org.java_websocket.WebSocket;
import users.exceptions.UserAddException;
import users.exceptions.UsersWebSocketIsNullException;
import utils.D;

import java.util.HashMap;
import java.util.Map;


public class Users {

    private static Users instance = new Users();
    Map<WebSocket, User> users = new HashMap<WebSocket, User>();

    private Users() {
    }

    public static Users getInstance() {
        return instance;
    }

    public int getUsersCount() {
        return users.size();
    }

    public synchronized User removeUser(WebSocket webSocket) {
        if (null == webSocket) {
            throw new UsersWebSocketIsNullException("Users.removeUser(WebSocket) websocket is null");
        }

        getUserByWebSocket(webSocket).destroy();
        D.debug("connection with user " + webSocket + " closed, user removed from server");
        D.debug("Users count : " + getUsersCount());
        return users.remove(webSocket);
    }

    public synchronized User removeUser(User user) {
        if (null == user) {
            throw new UserIsNullException("Users.removeUser(User) user is null");
        }
        user.destroy();
        D.debug("connection with user " + user + " closed, user removed from server");
        D.debug("Users count : " + getUsersCount());
        return users.remove(user.getWebSocket());
    }

    public synchronized User addUser(WebSocket webSocket) {
        if (users.get(webSocket) != null) {
            throw new UserAddException("User already exist");
        }
        User user = new User(webSocket);
        users.put(webSocket, user);
        D.debug("Users count : " + getUsersCount());
        return user;
    }

    public User getUserByWebSocket(WebSocket webSocket) {
        if (null == webSocket) {
            throw new UsersWebSocketIsNullException("Users.getUserByWebSocket(WebSocket) websocket is null");
        }
        User user = users.get(webSocket);
        if (null == user) {
            throw new UserIsNullException("Users.getUserByWebSocket(WebSocket) user is null");
        }
        return user;
    }

    public void sendToAll(String msg) {
        synchronized (users.entrySet()) {
            for (Map.Entry<WebSocket, User> entry : users.entrySet()) {
                entry.getKey().send(msg);
            }
        }
    }

}