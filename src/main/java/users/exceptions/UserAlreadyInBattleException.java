package users.exceptions;

public class UserAlreadyInBattleException extends RuntimeException {

    public UserAlreadyInBattleException(String s) {
        super(s);
    }

}
