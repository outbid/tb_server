package users.exceptions;


public class UserAddException extends RuntimeException {
    public UserAddException(String s) {
        super(s);
    }
}
