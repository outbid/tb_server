package users.exceptions;


public class UsersWebSocketIsNullException extends RuntimeException {
    public UsersWebSocketIsNullException(String s) {
        super(s);
    }
}
