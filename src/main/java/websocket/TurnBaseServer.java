package websocket;

import controller.interfaces.IRequestController;
import org.java_websocket.WebSocket;
import org.java_websocket.framing.Framedata;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import users.Users;
import utils.D;

import java.net.InetSocketAddress;
import java.net.UnknownHostException;


public class TurnBaseServer extends WebSocketServer {

    private IRequestController requestController;

    public IRequestController getRequestController() {
        return requestController;
    }

    public void setRequestController(IRequestController requestController) {
        this.requestController = requestController;
    }

    public TurnBaseServer(int port) throws UnknownHostException {
        super(new InetSocketAddress(port));
    }

    public TurnBaseServer(InetSocketAddress address) {
        super(address);
    }

    @Override
    public void onOpen(WebSocket conn, ClientHandshake handshake) {
        D.debug(conn.getRemoteSocketAddress().getAddress().getHostAddress() + " client connected");
        conn.send("Connection established");
    }

    @Override
    public void onClose(WebSocket conn, int code, String reason, boolean remote) {
        try {
            Users.getInstance().removeUser(conn);
            D.error("TurnBaseServer.onClose user removed from server");
        } catch (RuntimeException e) {
            D.debug("TurnBaseServer.onClose RuntimeException: " + e.getMessage());
        }
    }

    @Override
    public void onMessage(WebSocket conn, String message) {
        requestController.processRequest(conn, message);
    }

    public void onFragment(WebSocket conn, Framedata fragment) {
        D.debug("received fragment: " + fragment);
    }

    @Override
    public void onError(WebSocket conn, Exception ex) {
        ex.printStackTrace();
        if (conn != null) {
            conn.send("connection error: " + ex.getMessage());
        }
    }

    public void sendToAll(String text) {
        Users.getInstance().sendToAll(text);
    }

}