package websocket;

import net.minidev.json.JSONObject;
import users.User;

public class Request {

    private JSONObject request;
    private User from;

    public Request(User from, JSONObject request) {

        this.from = from;
        this.request = request;
    }

    public JSONObject getRequest() {
        return request;
    }

    public User getFrom() {
        return from;
    }

    public String toString() {
        return "User : " + from.getIp() + ", request: " + request.toString();
    }
}
