package utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class D {

    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");

    public static void debug(Object o) {
        System.out.println("[" + getTime() + "]" + o);
    }

    public static void error(String s) {
        System.err.println("[" + getTime() + "]" + s);
    }

    public static String getTime() {
        LocalDateTime timePoint = LocalDateTime.now();
        return timePoint.format(formatter);
    }
}
